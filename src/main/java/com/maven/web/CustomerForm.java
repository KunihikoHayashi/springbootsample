package com.maven.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

import com.maven.constant.Gender;

public class CustomerForm {
	@NotEmpty(message="姓は入力必須です。")
	@Size(min=1, max=10, message="姓は{min}以上{max}以下の文字数で入力してください。")
	private String lastName;
	@NotEmpty(message="名は入力必須です。")
	@Size(min=1, max=20, message="名は{min}以上{max}以下の文字数で入力してください。")
	private String firstName;
	@NotNull
	private Gender gender;

    public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
