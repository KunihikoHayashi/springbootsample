/*application.ymlで、例えばH2の場合、デフォルトでjpa.hibernate.ddl-auto: create-dropの様子なので、本ファイルのDDLは不要*/
/*application.ymlで、例えばH2の場合、かつ、jpa.hibernate.ddl-auto: noneの場合、本ファイルのDDLは必要*/
/*application.ymlで、例えばH2の場合、かつ、jpa.hibernate.ddl-auto: create-dropの場合、本ファイルのDDLを有効にすると、
 * data.sqlのデータが消えてしまうため、本ファイルは効果をなくすため、schema.sqlではなく、schema_bk.sqlとしておく。
 */
/*
CREATE TABLE IF NOT EXISTS Customers (
id INT PRIMARY KEY AUTO_INCREMENT,
 first_name VARCHAR(30),
 last_name VARCHAR(30)
 );
*/