package com.maven;


//import com.maven.conf.AppConfig;
//import com.maven.app.Argument;
//import com.maven.app.ArgumentResolver;
//import com.maven.app.Calculator;
//import com.maven.app.Frontend;
import com.maven.domain.Customer;
import com.maven.repository.CustomerRepository;
import com.maven.service.CustomerService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Import;

import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.util.Scanner;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

//@RestController
//@Import(AppConfig.class)//P30,31 @COnfigurationアノテーションをつけた対象のクラスを指定。設定ファイル読み込みのため。
//@EnableAutoConfiguration//P14 様々な設定が自動で行われるアノテーション
//@ComponentScan //P37 Componentアノテーションの持つBeans(DI)用のインジェクションしたいクラスを自動でサーチするアノテーション 
//@SpringBootApplicationの@Inheritedは以下のように継承先のクラスにのみアノテーションを継承する。（ただし、フィールドやメソッドにはアノテーションは継承しない。）
//http://unageanu.hatenablog.com/entry/20100712/1278946999
//@SpringBootApplicationの@CompopnentScanはそのアノテーションがあるクラスのパッケージ以下を捜査して@Component等を探す
@SpringBootApplication
public class App
{
	//***start application 3.1 P74***
	public static void main( String[] args ) {
		SpringApplication.run( App.class, args);
	}
	//***end application 3.1 P74***

}
