package com.maven.api;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.maven.domain.Customer;
import com.maven.service.CustomerService;

@RestController
@RequestMapping("api/customers")
public class CustomerRestController {
	@Autowired
	CustomerService customerService;

	//顧客全件取得 P78
	//curl http://localhost:8080/api/customers -v -X GET
	//@RequestMapping(method = RequestMethod.GET)
	//List<Customer> getCustomers() {
	//	List<Customer> customers = customerService.findAll();
	//	return customers;
	//}

	//顧客全件取得 P86
	//curl -X GET "http://localhost:8080/api/customers" →page=0,size=10
	//curl -X GET "http://localhost:8080/api/customers?page=0&size=3"
	//curl -X GET "http://localhost:8080/api/customers?page=1&size=3"
	@RequestMapping(method = RequestMethod.GET)
	Page<Customer> getCustomers(@PageableDefault Pageable pageable) {
		Page<Customer> customers = customerService.findAll(pageable);
		return customers;
	}

	//顧客1件取得
	//curl http://localhost:8080/api/customers/{1} -v -X GET
	//P78 @PathVariable→URLの/の後の動的なパラメータを受け取る時に使う。@RequestMappingのvalueと合うようにしなくてはならない。
	//(参考1)http://blog.codebook-10000.com/entry/20140301/1393628782
	//(参考2)http://kamatama41.hatenablog.com/entry/20130411/1365668200
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	Customer getCustomer(@PathVariable Integer id) {
		Customer customer = customerService.findOne(id);
		return customer;
	}

	//顧客新規作成 P81
	//curl http://localhost:8080/api/customers/ -v -X POST -H "Content-Type: application/json" -d "{\"firstName\" : \"Tamako\",  \"lastName\" : \"Nobi\" }"
	//@RequestMapping(method = RequestMethod.POST)
	//@ResponseStatus(HttpStatus.CREATED)
	//Customer postCustomer(@RequestBody Customer customer) {
	//	return customerService.create(customer);
	//}

	//顧客新規作成 P85
	//curl http://localhost:8080/api/customers/ -v -X POST -H "Content-Type: application/json" -d "{\"firstName\" : \"Tamako\",  \"lastName\" : \"Nobi\" }"
	//curlコマンド http://www.hcn.zaq.ne.jp/___/unix/curl_manual.html
	//-vオプション  curlコマンドで送信情報の詳細を表示
	//-X POST -d @- →-Xは--requestと等価らしく、GETかPOSTかの指定のイメージ。-dの後は、リクエストパラメータ。
	//-Xオプション -Xは--requestと等価
	//-Hオプション 独自ヘッダを渡す際に使用。今回は、Content-Type。
	//-dオプション リクエストパラメータの指定
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> postCustomers(@RequestBody Customer customer, UriComponentsBuilder uriBuilder) {
		Customer created = customerService.create(customer);
		URI location = uriBuilder.path("api/customers/{id}").buildAndExpand(created.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(location);
		return new ResponseEntity<>(created,headers,HttpStatus.CREATED);
	}

	//顧客1件更新
	//curl http://localhost:8080/api/customers/1 -v -X PUT -H "Content-Type: application/json" -d "{\"firstName\" : \"Nobio\",  \"lastName\" : \"Nobi\" }"
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	Customer putCustomer(@PathVariable Integer id,@RequestBody Customer customer) {
		customer.setId(id);
		return customerService.update(customer);
	}

	//顧客1件削除
	//curl http://localhost:8080/api/customers/1 -v -X DELETE
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	void deleteCustomer(@PathVariable Integer id) {
		customerService.delete(id);
	}

	
}
