/* P50 SpringBootではクラスパス直下にdata.sql,schema.sql等のある名前のsqlファイルを置くと初期データとして読み込んで実行される*/ 
INSERT INTO customers(first_name,last_name,gender) VALUES('Nobita','Nobi',0);
INSERT INTO customers(first_name,last_name,gender) VALUES('Takeshi','Goda',0);
INSERT INTO customers(first_name,last_name,gender) VALUES('Suneo','Honekawa',0);
INSERT INTO customers(first_name,last_name,gender) VALUES('Shizuka','Minamoto',1);
